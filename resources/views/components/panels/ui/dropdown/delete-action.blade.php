<li>
    <form {{ $attributes->merge(['method' => 'POST']) }}>
        @method('DELETE')
        @csrf
        <button type="submit" class="dropdown-item">Удалить</button>
    </form>
</li>
