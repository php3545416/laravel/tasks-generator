@props([
    'task',
    'action',
    'method',
    'parameters',
])

<x-layouts.admin>
    @push('styles')
        <style>
            #draggablePanelList .panel-heading {
                cursor: move;
            }
            #draggablePanelList2 .panel-heading {
                cursor: move;
            }

        </style>
    @endpush

    <script type="module">
        const el = document.getElementById('draggablePanelList');
        const sortable = makeElSortable(el);

        $(document).on('click', "svg[name='remove_input']", function (event) {
            $(event.currentTarget).parents('li').remove();
        });

        $('select').change(async function () {
            const selected = $('select option:selected');

            const { data } = await axios.get('/admin/tasks/linked-parameter-view', { params: {
                value: selected.val(),
                text: selected.text(),
            }})

            let lastLi = $("#draggablePanelList li:last-child");

            if (!lastLi.length) {
                $($.parseHTML(data.view)).appendTo(el);
            } else {
                lastLi.after(data.view);
            }
        });
    </script>

    <x-slot name="title">{{  $task ? 'Изменить задачу' : 'Добавить задачу' }}</x-slot>

    <div class="container">
        <h4 class="text-center mb-4 mt-3"> {{  $task ? 'Изменить задачу' : 'Добавить задачу' }}</h4>

        <form method="POST" action="{{ $action }}">
            @csrf
            @method($method)

            <x-panels.forms.group name="number">
                <label class="fw-bold form-label" for="number">Номер задания</label>
                <input class="form-control @error('number') is-invalid @enderror" id="number" name="number" value="{{ old('number', $task?->number) }}" placeholder="Введите номер" required>
            </x-panels.forms.group>

            <x-panels.forms.group name="description">
                <label class="fw-bold form-label" for="description">Описание</label>
                <input class="form-control @error('description') is-invalid @enderror" id="description" name="description" value="{{ old('description', $task?->description) }}" placeholder="Задача из раздела ...">
            </x-panels.forms.group>

            <x-panels.forms.group name="description">
                <label class="fw-bold form-label" for="conditions_template">Шаблон условия</label>
                <textarea class="form-control @error('conditions_template') is-invalid @enderror" id="conditions_template" name="conditions_template" rows="6" placeholder="Введите шаблона условия задачи" required>{{ old('conditions_template', $task?->conditions_template) }}</textarea>
            </x-panels.forms.group>

            <x-panels.forms.group name="parameters">
                <label class="fw-bold form-label" for="conditions_template">Добавить переменную</label>

                <select id="parameters" class="form-select">
                    @foreach($parameters as $parameter)
                        <option value="{{ $parameter->id }}">{{ $parameter->full_name }}</option>
                    @endforeach
                </select>
            </x-panels.forms.group>

            <x-panels.forms.group name="linked_params">
                <label class="fw-bold form-label" for="conditions_template">Привязанные переменные <i>(в порядке указания в условии задачи)</i></label>

                <ul id="draggablePanelList" class="list-group">
                    @if($task?->parameters)
                        @foreach($task->parameters as $linked)
                            <x-tasks.linked-parameter :linked="$linked"/>
                        @endforeach
                    @endif
                </ul>
            </x-panels.forms.group>

            <x-panels.forms.group name="answer_template">
                <label class="fw-bold form-label" for="answer_template">Формула решения <i>(привязка переменной к формуле - %n$s, где n - порядковый номер переменной в условии). В силу технических ограничений, пример указания в шаблонах основания логарифма: log(2, 4) = log(2) / log(4)</i></label>
                <textarea class="form-control @error('answer_template') is-invalid @enderror" id="answer_template" name="answer_template" rows="6" placeholder="Введите шаблон решения задачи" required>{{ old('answer_template', $task?->answer_template) }}</textarea>
            </x-panels.forms.group>

            <x-panels.forms.group name="algorithm_template">
                <label class="fw-bold form-label" for="algorithm_template">Агоритм решения <i>(привязка переменной к алгоритму - %n$s, где n - порядковый номер переменной в условии; привязка ответа - ключевое слово answer)</i></label>
                <textarea class="form-control @error('algorithm_template') is-invalid @enderror" id="algorithm_template" name="algorithm_template" rows="6" placeholder="Введите шаблон алгоритма решения задачи" required>{{ old('algorithm_template', $task?->algorithm_template) }}</textarea>
            </x-panels.forms.group>

            <x-panels.forms.actions />
        </form>
    </div>
</x-layouts.admin>
