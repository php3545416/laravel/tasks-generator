<x-layouts.guest>
    <slot name="title">Восстановление пароля</slot>

    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-floating my-3">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" placeholder="Введите почту" required autocomplete="email" autofocus>
            <label for="email">Адрес электронной почты</label>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-floating my-3">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="*******" required autocomplete="new-password">
            <label for="password">Новый пароль</label>

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-floating my-3">
            <input id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="*******" required autocomplete="new-password">
            <label for="password-confirm">Подтвердите пароль</label>

            @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <button class="btn btn-success w-100 py-2" type="submit">Изменить пароль</button>
    </form>
</x-layouts.guest>
