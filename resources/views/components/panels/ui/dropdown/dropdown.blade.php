@props(['title'])
<div class="dropdown">
    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
        {{ $title }}
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        {{ $slot }}
    </ul>
</div>
