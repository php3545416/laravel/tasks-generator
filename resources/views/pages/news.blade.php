@props(['news'])

<x-layouts.app>
    <x-slot name="title">Новости проекта</x-slot>

    <x-slot name="mainSlot">
        <p class="h4 text-center mt-3">Новости проекта</p>
        <div class="container out mt-3">
            <table class="table">
                @foreach($news as $item)
                    <x-news.news-item :news="$item" />
                @endforeach
            </table>
        </div>
    </x-slot>
</x-layouts.app>
