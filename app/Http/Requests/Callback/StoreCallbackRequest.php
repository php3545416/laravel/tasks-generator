<?php

namespace App\Http\Requests\Callback;

use Illuminate\Foundation\Http\FormRequest;

class StoreCallbackRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'theme' => 'required|string',
            'description' => 'required|string',
        ];
    }
}
