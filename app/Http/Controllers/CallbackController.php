<?php

namespace App\Http\Controllers;

use App\Http\Requests\Callback\StoreCallbackRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class CallbackController extends Controller
{
    public function index(): View
    {
        return view('pages.callback');
    }

    public function store(StoreCallbackRequest $request): RedirectResponse
    {
        if (!$request->user()) {
            Session::flash('error_messages', 'Для отправки заявки необходимо авторизоваться!');
            return back();
        }

        if (!empty($request->validated())) {
            $user = $request->user();
            $data = $request->validated();

            Session::flash('success_messages', 'Спасибо, что оставили заявку. Вы делаете нас лучше!');
            Log::info("Пользователь ($user->name (id: $user->id, email: $user->email, phone: $user->phone) оставил заявку на сайте. Тема: {$data['theme']}. Описание: {$data['description']}.");
        }

        return back();
    }
}
