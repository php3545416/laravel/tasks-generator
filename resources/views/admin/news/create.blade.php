@props([
    'news',
    'action',
    'method',
])

<x-layouts.admin>
    <x-slot name="title">{{ $news ? 'Изменить новость' : 'Создать новость' }}</x-slot>

    <div class="container">
        <h4 class="text-center mb-4">{{ $news ? 'Изменить новость' : 'Создать новость' }}</h4>
        <form method="POST" action="{{ $action }}" enctype="multipart/form-data">
            @csrf
            @method($method)

            <x-panels.forms.group name="link">
                <label for="link">Ссылка</label>
                <input type="url" class="form-control @error('link') is-invalid @enderror" id="link" name="link" value="{{ old('link', $news?->link) }}" placeholder="Введите ссылку" required>
            </x-panels.forms.group>

            <x-panels.forms.group name="title">
                <label for="title">Название</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title', $news?->title) }}" placeholder="Введите название" required>
            </x-panels.forms.group>

            <x-panels.forms.group name="description">
                <label for="description">Описание</label>
                <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" rows="3" placeholder="Введите описание" required>{{ old('description', $news?->description) }}</textarea>
            </x-panels.forms.group>

            <x-panels.forms.group name="date">
                <label for="date">Дата</label>
                <input type="date" class="form-control @error('description') is-invalid @enderror" id="date" name="date" value="{{ old('date', $news?->date) }}" placeholder="Введите дату публикации" required>
            </x-panels.forms.group>

            <x-panels.forms.group name="image">
                <label for="image">Изображение</label>

                <input class="form-control @error('image') is-invalid @enderror" id="image" name="image" type="file" value="{{ old('title', $news?->title) }}">

                @if(old('image', $news?->image_url))
                    <div class="my-4">
                        <img class="w-25 h-25" src="{{ old('image', $news?->image_url) }}" alt="{{ old('title', $news?->title) }}"/>
                    </div>
                @endif
            </x-panels.forms.group>

            <x-panels.forms.actions />
        </form>
    </div>
</x-layouts.admin>
