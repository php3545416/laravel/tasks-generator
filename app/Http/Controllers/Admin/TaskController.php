<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TaskCreateRequest;
use App\Http\Requests\GetLinkedParameterViewRequest;
use App\Models\Parameter;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class TaskController extends Controller
{
    public function create(): View
    {
        return view('admin.tasks.create', [
            'task' => null,
            'action' => route('admin.tasks.store'),
            'method' => 'POST',
            'parameters' => Parameter::all(),
        ]);
    }

    public function store(TaskCreateRequest $request): RedirectResponse
    {
        $task = Task::query()->create($request->safe()->except('linked_params'));

        if ($task) {
            $task->syncParameters($request->validated('linked_params', []));

            session()->flash('success_messages', 'Задание создано');
        } else {
            session()->flash('error_messages', 'Не удалось создать задание');

            return redirect(route('admin.tasks.create'));
        }

        return redirect(route('admin.index'));
    }

    public function edit(int $id): View
    {
        return view('admin.tasks.create', [
            'task' => $task = Task::query()->with(['parameters'])->findOrFail($id),
            'action' => route('admin.tasks.update', compact('task')),
            'method' => 'PUT',
            'parameters' => Parameter::all(),
        ]);
    }

    public function update(int $id, TaskCreateRequest $request): RedirectResponse
    {
        $task = Task::query()->findOrFail($id);

        $updated = $task->update($request->safe()->except(['linked_params']));

        if ($updated) {
            $task->syncParameters($request->validated('linked_params', []));
            session()->flash('success_messages', 'Задание обновлено');
        } else {
            session()->flash('error_messages', 'Не удалось обновить задание');
        }

        return back();
    }

    public function destroy(int $id): RedirectResponse
    {
        Task::query()->where('id', $id)->delete();

        return redirect(route('admin.index'));
    }

    public function getLinkParameterView(GetLinkedParameterViewRequest $request): JsonResponse
    {
        if (! $request->expectsJson()) {
            throw new BadRequestException();
        }

        $task = new Task();
        $task->id = $request->validated('value');
        $task->full_name = $request->validated('text');

        return response()->json(['view' => view('components.tasks.linked-parameter', ['linked' => $task])->render()]);
    }
}
