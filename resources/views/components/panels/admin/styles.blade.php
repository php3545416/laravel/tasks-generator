@push('styles')
    <style>
        body {
            height: 80vh;
        }
        .container {
            max-width: 700px;
            margin: auto;
            margin-top: 50px;
            padding: 30px;
            border: 1px solid #ddd;
            border-radius: 10px;
        }
        .container h4 {
            font-size: 1.5rem;
        }
        .container .form-control {
            font-size: 1.2rem;
            padding: 10px;
        }
        .container .btn {
            font-size: 1.1rem;
            padding: 10px 20px;
        }
    </style>
@endpush
