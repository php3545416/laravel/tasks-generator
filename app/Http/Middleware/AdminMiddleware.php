<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminMiddleware
{
    public function handle(Request $request, Closure $next): Response
    {
        if (! $request->user() || ! auth()->user()->hasRole('admin')) {
            return redirect(RouteServiceProvider::HOME);
        }

        return $next($request);
    }
}
