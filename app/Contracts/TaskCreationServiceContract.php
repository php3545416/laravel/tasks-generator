<?php

namespace App\Contracts;

use Illuminate\Support\Collection;

interface TaskCreationServiceContract
{
    public function createTasksByTemplate(array $taskIds, array $idsWithCount): Collection;
}
