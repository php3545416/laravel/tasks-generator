@props(['tasks'])
<x-layouts.app>
    <x-slot name="title">Сгенерированные задачи</x-slot>

    <p class="h4 text-center mt-3 my-5">Сгенерированные задачи</p>
    <div class="mt-3">
        @foreach($tasks as $task)
            <x-tasks.task-item :task="$task"/>
        @endforeach
    </div>
</x-layouts.app>
