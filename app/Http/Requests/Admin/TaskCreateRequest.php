<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TaskCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'number' => 'required|integer|gt:0',
            'description' => 'nullable|string',
            'algorithm_template' => 'required|string',
            'answer_template' => 'required|string',
            'conditions_template' => 'required|string',
            'linked_params' => 'nullable|array',
            'linked_params.*' => 'required|integer',
        ];
    }
}
