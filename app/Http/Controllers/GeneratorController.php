<?php

namespace App\Http\Controllers;

use App\Contracts\TaskCreationServiceContract;
use App\Http\Requests\GenerateTaskRequest;
use App\Models\Task;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Throwable;

class GeneratorController extends Controller
{
    public function __construct(
        private readonly TaskCreationServiceContract $service,
    ) {
    }

    public function index(): View
    {
        return view('pages.generator', [
            'tasks' => Task::all()->groupBy(['number', 'description']),
        ]);
    }

    public function store(GenerateTaskRequest $request): View|RedirectResponse
    {
        $selectedIds = $request->validated('selected_ids', []);

        try {
            $tasks = $this->service->createTasksByTemplate(
                $selectedIds,
                $request->validated('selected', []),
            );
        } catch (Throwable $e) {
            session()->flash('error_messages', $e->getMessage());
            return back();
        }

        return view('pages.generated', ['tasks' => $tasks]);
    }
}
