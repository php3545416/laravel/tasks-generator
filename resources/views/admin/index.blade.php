@props([
    'tasks',
    'news',
])
<x-layouts.admin>
    <x-slot name="title">Панель администратора</x-slot>

    <p class="h4 text-center mt-3">Панель администратора</p>

    <x-panels.admin.news-list :news="$news" />
    <x-panels.admin.tasks-list :tasks="$tasks" />
</x-layouts.admin>
