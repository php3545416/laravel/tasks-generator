<x-layouts.app>
    <x-slot name="title">Обратная связь</x-slot>

    @push('styles')
        <style>
            .h {
                height: 80vh;
            }
            .form-container {
                max-width: 1000px;
                padding: 15px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
        </style>
    @endpush

    <x-slot name="mainSlot">
        <div class="d-flex align-items-center justify-content-center h">
            <main class="form-container">
                <p class="h4 text-center mt-3">Заявка на обратный звонок</p>
                <form method="POST" action="{{ route('callback.store') }}" class="row g-3">
                    @csrf
                    @method('POST')

                    <x-panels.forms.group name="theme">
                        <label for="theme">Тема</label>
                        <input id="theme" name="theme" type="text" class="form-control @error('theme') is-invalid @enderror" value="{{ old('theme') }}" placeholder="Введите тему сообщения">
                    </x-panels.forms.group>

                    <x-panels.forms.group name="description">
                        <label for="description">Описание</label>
                        <textarea id="description" name="description" value="{{ old('description') }}" class="form-control" rows="3" placeholder="Введите вопрос , задачу или пожелание и нажмите отправить." required></textarea>
                    </x-panels.forms.group>

                    <div class="col-12">
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </div>
                </form>
            </main>
        </div>
    </x-slot>
</x-layouts.app>
