<x-layouts.guest>
    <x-slot name="title">Регистрация</x-slot>

    <form method="POST" action="{{ route('register') }}">
        @csrf

        <h1 class="h3 mb-3 fw-normal">Введите данные для регистрации</h1>

        <div class="form-floating my-3">
            <input id="last_name" name="last_name" class="form-control @error('last_name') is-invalid @enderror" placeholder="Фамилия" value="{{ old('last_name') }}" required>
            <label for="last_name">Фамилия</label>

            @error('last_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-floating my-3">
            <input id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Имя" value="{{ old('name') }}" required>
            <label for="name">Имя</label>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-floating my-3">
            <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="example@example.com" value="{{ old('email') }}" required>
            <label for="email">Адрес электронной почты</label>

            @error('email')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-floating my-3">
            <input id="password" type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="********" required>
            <label for="password">Пароль</label>

            @error('password')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-floating my-3">
            <input id="password_confirmation" type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="********" required>
            <label for="password_confirmation">Повторите пароль</label>

            @error('password_confirmation')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <button class="btn btn-success w-100 py-2" type="submit">Зарегистрироваться</button>
    </form>
</x-layouts.guest>
