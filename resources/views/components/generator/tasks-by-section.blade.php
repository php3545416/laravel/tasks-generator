@props(['section', 'tasks'])

<li class="checkbox-wrapper">
    <p class="h6" style="width: 20%;">{{ $section }}</p>
    <ul class="nested-list my-4" style="width: 80%;">
        @foreach($tasks as $index => $task)
            <x-generator.task-item :task="$task" />
        @endforeach
    </ul>
</li>
