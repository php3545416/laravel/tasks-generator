@props(['task'])

<div class="form-check-inline small" data-mdb-input-init>
    <input type="button" onclick="handleCounterMinus(this)" value="-" class="button-minus">
    <input type="number" step="1" max="100" value="0" name="tasks_count[<?=$task['id']?>]" class="quantity-field">
    <input type="button" onclick="handleCounterPlus(this)" value="+" class="button-plus">
</div>
