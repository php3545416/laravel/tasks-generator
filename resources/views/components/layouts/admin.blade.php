@props([
    'title',
])
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<x-parts.head :title="$title ?? config('app.name', 'Laravel')"/>
<body class="align-items-center justify-content-center">

<x-parts.header />

<div class="container my-4">
    <x-panels.messages.flashes />
</div>

{{ $slot ?? '' }}

{{--<div class="container d-grid gap-2" style="border: none; margin-top: 10px;">--}}
{{--    <a class="btn btn-outline-secondary" href='{{ route('home') }}'>Вернуться назад</a>--}}
{{--</div>--}}

<x-parts.footer />
</body>
</html>
