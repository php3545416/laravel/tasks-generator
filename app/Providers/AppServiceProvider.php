<?php

namespace App\Providers;

use App\Contracts\TaskCreationServiceContract;
use App\Services\TaskCreationService;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(TaskCreationServiceContract::class, TaskCreationService::class);
    }

    public function boot(): void
    {
        Blade::if('admin', fn () => Gate::allows('admin'));
    }
}
