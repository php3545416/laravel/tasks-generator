@props(['task'])

@push('styles')
    <style>
        .otvet {
            text-align: start;

        }
    </style>
@endpush

<div class="container out">
    <p>{{ $task->condition }}</p>

    <div class="d-flex justify-content-between align-items-center">
        <div>
            <p id="{{ $task->id }}" class="otvet mb-0 d-none">{{ 'Ответ: ' . $task->answer }}</p>
        </div>
        <div>
            <button data-id="{{ $task->id }}" onclick="showAnswer(this)" class="btn btn-outline-warning me-md-2" type="button">Ответ</button>
            <!-- Кнопка-триггер модального окна -->
            <button type="button" class="btn btn-outline-info" data-bs-toggle="modal" data-bs-target="#{{ 'exampleModal' . $task->id }}">
                Решение
            </button>

            <!-- Модальное окно -->
            <div class="modal fade" id="{{ 'exampleModal' . $task->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">Теоретическая часть</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
                        </div>
                        <div class="modal-body">
                            <div class="fw-bold">Условие:</div>
                            <div><p>{{ $task->condition }}</p></div>

                            <div class="fw-bold">Алгоритм решения:</div>
                            {{ $task->algorithm_template }}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Понятно</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
