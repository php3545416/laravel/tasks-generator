<nav class="navbar navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand" href='{{ route('home') }}'>Генератор</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Переключатель навигации">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
            <div class="navbar-nav nav-underline">
                <a class="nav-link @if (request()->routeIs('home')) active @endif" href='{{ route('home') }}'>Главная</a>
                <a class="nav-link @if (request()->routeIs('generator.index')) active @endif" href='{{ route('generator.index') }}'>Генератор</a>
                <a class="nav-link @if (request()->routeIs('news.index')) active @endif" href='{{ route('news.index') }}'>Новости</a>
                <a class="nav-link @if (request()->routeIs('callback.index')) active @endif" href='{{ route('callback.index') }}'>Обратная связь</a>
                @admin
                <a class="nav-link @if (str()->contains(request()->route()->getPrefix(), 'admin')) active @endif" href='{{ route('admin.index') }}'>Админ. панель</a>
                @endadmin
            </div>
        </div>
        <form class="justify-content-end">
            @guest
                @if (Route::has('login'))
                    <a href='{{ route('login') }}' class="btn btn-outline-success" type="button">Войти</a>
                @endif
                @if (Route::has('register'))
                    <a href='{{ route('register') }}' class="btn btn-outline-secondary" type="button">Регистрация</a>
                @endif
            @else
                <div class="h5">
                    <a href='{{ route('profile.index') }}' class="link-dark link-underline-opacity-0">Личный кабинет</a>
                </div>
            @endguest
        </form>
    </div>
</nav>
