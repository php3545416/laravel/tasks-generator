@props(['news'])

<tr>
    <th scope="row">{{ $news->id }}</th>
    <td><img src="{{ $news->image_url }}" class="img-thumbnail w-50 h-50" alt="{{ $news->title }}"></td>
    <td>{{ $news->title }}</td>
    <td>{{ $news->description }}</td>
    <td>{{ $news->date }}</td>
    <td>
        <x-panels.ui.dropdown.dropdown title="Действия">
            <x-panels.ui.dropdown.update-action :action="route('admin.news.edit', $news->id)" />
            <x-panels.ui.dropdown.delete-action :action="route('admin.news.destroy', $news->id)" />
        </x-panels.ui.dropdown.dropdown>
    </td>
</tr>
