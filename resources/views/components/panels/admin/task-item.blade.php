@props(['task'])

<tr>
    <th scope="row">{{ $task->id }}</th>
    <td>{{ $task->number }}</td>
    <td>{{ $task->conditions_template }}</td>
    <td>
        <x-panels.ui.dropdown.dropdown title="Действия">
            <x-panels.ui.dropdown.update-action :action="route('admin.tasks.edit', $task->id)" />
            <x-panels.ui.dropdown.delete-action :action="route('admin.tasks.destroy', $task->id)" />
        </x-panels.ui.dropdown.dropdown>
    </td>
</tr>
