@props([
    'title',
])
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<x-parts.head :title="$title ?? config('app.name', 'Laravel')"/>
<body style="height: 80vh" class="d-flex align-items-center justify-content-center">
    <main class="form-signin">
        {{ $slot ?? '' }}
    </main>

    <x-parts.footer class="fixed-bottom" />
</body>
</html>
