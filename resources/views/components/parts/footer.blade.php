<footer {{ $attributes }} >
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
        <li class="nav-item"><a href='{{ route('home') }}' class="nav-link px-2 text-muted">Главная</a></li>
        <li class="nav-item"><a href='{{ route('generator.index') }}' class="nav-link px-2 text-muted">Генератор</a></li>
        <li class="nav-item"><a href='{{ route('news.index') }}' class="nav-link px-2 text-muted">Новости</a></li>
        <li class="nav-item"><a href='{{ route('callback.index') }}' class="nav-link px-2 text-muted">Обратная связь</a></li>
    </ul>
    <p class="text-center text-muted">© 2024 ВолГУ, Прикладная информатика</p>
</footer>
