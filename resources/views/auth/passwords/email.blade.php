<x-layouts.guest>
    <x-slot name="title">Восстановление пароля</x-slot>

    <h1 class="h3 mb-3 fw-normal">Восстановление пароля</h1>

    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="form-floating my-3">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" placeholder="example@email.com">

            <label for="email">Адрес электронной почты</label>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <!-- Кнопка-триггер модального окна -->
        <button type="submit" class="btn btn-success w-100 py-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Отправить
        </button>

        <!-- Модальное окно -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Уведомление</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
                    </div>
                    <div class="modal-body">
                        Пароль будет отправлен на емаил
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Понятно</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</x-layouts.guest>
