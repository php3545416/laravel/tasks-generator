@props(['linked'])
<li class="panel panel-info list-group-item">
    <div class="d-inline-flex">
        <x-panels.ui.dragndrop.switch-svg />
    </div>

    <div class="d-inline-block">
        <input name="linked_params[]" type="hidden" value="{{ $linked['id'] }}" />
        <div class="panel-heading">{{ $linked['full_name'] }}</div>
    </div>

    <div class="d-inline-block">
        <x-panels.ui.dragndrop.remove-svg :id="$linked['id']" name="remove_input" />
    </div>
</li>
