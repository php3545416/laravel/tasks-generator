import 'jquery/dist/jquery.min.js'
import jQuery from 'jquery';
import './bootstrap';
import '@popperjs/core/dist/umd/popper.min.js';
import Sortable from "sortablejs";

export function makeElSortable(el) {
    return Sortable.create(el, {
        animation: 150,
    });
}

export function showAnswer(el) {
    const tappedId = $(el).attr('data-id');

    const answer = $(`#${tappedId}`)

    if (answer.hasClass('d-none')) {
        answer.removeClass('d-none')
    } else {
        answer.addClass('d-none');
    }
}

function incrementValue(e) {
    const parent = $(e).closest('div');
    const counter = parent.find('input[type=number]');

    const currentVal = parseInt(counter.val(), 10);

    if (currentVal === 100) {
        counter.val(100);
    } else {
        counter.val(currentVal + 1)
    }
}

function decrementValue(e) {
    const parent = $(e).closest('div');
    const counter = parent.find('input[type=number]');

    const currentVal = parseInt(counter.val(), 10);

    if (currentVal === 0) {
        counter.val(0);
    } else {
        counter.val(currentVal - 1)
    }
}

export function handleCounterPlus(el) {
    incrementValue(el);
}

export function handleCounterMinus(el) {
    decrementValue(el);
}

window.makeElSortable = makeElSortable;
window.showAnswer = showAnswer;
window.handleCounterPlus = handleCounterPlus;
window.handleCounterMinus = handleCounterMinus;

window.$ = jQuery;
