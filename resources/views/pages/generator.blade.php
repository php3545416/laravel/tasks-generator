@props(['tasks'])

<x-layouts.app>
    <x-slot name="title">Генератор задач</x-slot>

    @push('styles')
        <style>
            .checkbox-wrapper {
                display: flex;
                align-items: center;
                width: 100%;
            }
            .checkbox-wrapper input[type="checkbox"] {
                margin-right: 10px;
            }
            .nested-list {
                list-style-type: none;
                padding-left: 20px;
                width: 80%;
            }
            .list-unstyled {
                padding-left: 0;
                display: flex;
                flex-direction: column;
                align-items: center;
                width: 100%;
            }
            .btn-container {
                margin-top: 20px;
                display: flex;
                justify-content: end;
            }
        </style>
    @endpush

    <form action="{{ route('generator.store') }}" method="POST">
        @csrf
        @method('POST')

        <div class="container mt-5">
            @foreach($tasks as $number => $tasks)
                <x-generator.tasks-by-number :number="$number" :tasks="$tasks" />
            @endforeach
        </div>

        <div class="container btn-container">
            <button type="submit" class="btn btn-primary btn-lg">Генерировать задачи</button>
        </div>
    </form>
</x-layouts.app>
