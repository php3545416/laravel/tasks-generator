<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->integer('number');
            $table->longText('answer_template');
            $table->longText('conditions_template');
            $table->longText('algorithm_template');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('parameters', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code');
            $table->string('measure')->nullable();
            $table->integer('rand_from')->nullable();
            $table->bigInteger('rand_to')->nullable();
            $table->integer('value')->nullable();
            $table->timestamps();
        });

        Schema::create('parameter_task', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parameter_id')->references('id')->on('parameters')->onDelete('cascade');
            $table->foreignId('task_id')->references('id')->on('tasks')->onDelete('cascade');
            $table->integer('number');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('parameter_task');
        Schema::dropIfExists('parameters');
        Schema::dropIfExists('tasks');

    }
};
