<?php

namespace Database\Seeders;

use App\Models\Parameter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ParametersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Parameter::query()->insert($this->prepare());
    }

    private function prepare(): array
    {
        $data = [];
        foreach ($this->values() as $id => $item) {
            $data[] = $this->addAdditionalKeys($id, array_combine($this->baseKeys(), $item));
        }

        return $data;
    }

    private function baseKeys(): array
    {
        return [
            'name',
            'measure',
            'rand_from',
            'rand_to',
            'value',
        ];
    }

    private function values(): array
    {
        return [
            ['Диапазон для пикселей', 'пиксель', 100, 1000, null],
            ['Диапазон для размера в Кбайт', 'Кбайт', 10, 1000, null],
            ['Диапазон для частоты дискретизации', 'кГц', 8, 192, null],
            ['Диапазон для разрешения в битах', 'бит', 8, 32, null],
            ['Диапазон для размера в Мбайт', 'Мбайт', 1, 1000, null],
            ['Диапазон для скорости передачи', 'бит/c', 1000, 1000000, null],
            ['Фиксированное значение для числа цифр в сообщении', null, null, null, 10],
            ['Диапазон для количества сообщений', null, 1, 1000, null],
            ['Диапазон для количества цветов в изображении', null, 1, 1000, null],
        ];
    }

    private function addAdditionalKeys(int $id, array $data): array
    {
        return [
            ...$data,
            'code' => Str::slug($data['name']),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
