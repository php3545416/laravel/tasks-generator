@props(['task'])

<li class="checkbox-wrapper my-4">
    <x-panels.ui.counter.counter :task="$task" />
    <input type="checkbox" name="selected_ids[]" class="child-checkbox" value="{{ $task->id }}">
    <label for="selected_ids[]">{{ $task->conditions_template }}</label>
</li>
