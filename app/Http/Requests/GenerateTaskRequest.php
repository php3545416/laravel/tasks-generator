<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GenerateTaskRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'selected_ids' => 'array',
            'selected_ids.*' => 'required|integer',
            'tasks_count' => 'array',
            'tasks_count.*' => 'required|integer',
            'selected' => 'array',
            'selected.*.id' => 'required|integer',
            'selected.*.count' => 'required|integer',
        ];
    }

    protected function prepareForValidation(): void
    {
        $selected = collect($this->selected_ids ?? [])
            ->mapWithKeys(function (int $id, int $index) {
                return [$index => ['id' => $id, 'count' => $this->tasks_count[$id] ?? 0]];
            })
            ->filter(fn ($item) => $item['count'])
            ->all();

        $this->merge(['selected' => $selected]);
    }
}
