@props([
    'user'
])
<x-layouts.app>
    <x-slot name="title">Личный кабинет</x-slot>

    @push('styles')
        <style>
            .h {
                height: 80vh;
            }
            .form-container {
                max-width: 1000px;
                padding: 15px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
        </style>
    @endpush

    <x-slot name="mainSlot">
        <div class="d-flex align-items-center justify-content-center h">
            <main class="form-container">
                <div class="d-inline-block">
                    <form method="post" action="{{ route('profile.update') }}" class="row g-3">
                        @csrf
                        @method('patch')

                        <div class="col-12">
                            <label for="name" class="form-label">ФИО</label>
                            <input id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Фамилия Имя Отчество" value="{{ old('name', $user->name) }}">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-12">
                            <label for="email" class="form-label">Эл. адрес</label>
                            <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="example@example.com" value="{{ old('email', $user->email) }}">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        {{--                    <div class="col-12">--}}
                        {{--                        <label for="password" class="form-label">Пароль</label>--}}
                        {{--                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="********" value="{{ $user->password }}">--}}

                        {{--                        @error('password')--}}
                        {{--                            <span class="invalid-feedback" role="alert">--}}
                        {{--                                <strong>{{ $message }}</strong>--}}
                        {{--                            </span>--}}
                        {{--                        @enderror--}}
                        {{--                    </div>--}}
                        <div class="col-12">
                            <label for="phone" class="form-label">Номер телефона</label>
                            <input class="form-control @error('password') is-invalid @enderror" id="phone" name="phone" placeholder="+7(950)-529-49-86" value="{{ old('phone', $user->phone) }}">

                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-12">
                            <label for="address" class="form-label">Адрес</label>
                            <input type="text" class="form-control @error('password') is-invalid @enderror" id="address" name="address" placeholder="Проспект Ленина" value="{{ old('phone', $user->address) }}">

                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-12 d-flex justify-content-between">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
                <div class="d-inline-block my-3">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button type="submit" class="btn btn-danger">Выйти из аккаунта</button>
                    </form>
                </div>
            </main>
        </div>
    </x-slot>
</x-layouts.app>
