<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsCreateRequest;
use App\Models\News;
use ErlandMuchasaj\LaravelFileUploader\Exceptions\UploadFailed;
use ErlandMuchasaj\LaravelFileUploader\FileUploader;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Throwable;

class NewsController extends Controller
{
    public function create(): View
    {
        return view('admin.news.create', [
            'news' => null,
            'action' => route('admin.news.store'),
            'method' => 'POST',
        ]);
    }

    /**
     * @throws UploadFailed
     * @throws Exception
     */
    public function store(NewsCreateRequest $request): RedirectResponse
    {
        $news = News::query()->create($request->safe()->except('image'));

        try {
            if (! $news) {
                throw new Exception();
            }

            if ($file = $request->file('image')) {
                $response = FileUploader::store($file);

                if ($response) {
                    $news->image = $response['path'];
                    $news->save();
                }
            }

            session()->flash('success_messages', 'Новость создана');
        } catch (Throwable $e) {
            throw $e;
            session()->flash('error_messages', 'Не удалось создать новость');

            return redirect(route('admin.news.create'));
        }

        return redirect(route('admin.index'));
    }

    public function edit(int $id): View
    {
        return view('admin.news.create', [
            'news' => $news = News::query()->findOrFail($id),
            'action' => route('admin.news.update', compact('news')),
            'method' => 'PUT',
        ]);
    }

    public function update(int $id, NewsCreateRequest $request): RedirectResponse
    {
        $news = News::query()->findOrFail($id);

        $updated = $news->update($request->safe()->except('image'));

        try {
            if (! $updated) {
                throw new Exception();
            }

            if ($file = $request->file('image')) {
                FileUploader::remove($news->image);

                $response = FileUploader::store($file);

                if ($response) {
                    $news->image = $response['path'];
                    $news->save();
                }
            }

            session()->flash('success_messages', 'Новость обновлена');
        } catch (Throwable $e) {
            session()->flash('error_messages', 'Не удалось обновить новость');
        }

        return back();
    }

    public function destroy(int $id): RedirectResponse
    {
        News::query()->where('id', $id)->delete();

        return redirect(route('admin.index'));
    }
}
