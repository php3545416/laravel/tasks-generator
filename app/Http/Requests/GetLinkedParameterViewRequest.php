<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetLinkedParameterViewRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'value' => 'required|integer',
            'text' => 'required|string',
        ];
    }
}
