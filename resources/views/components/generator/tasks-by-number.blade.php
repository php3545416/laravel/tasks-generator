@props(['number', 'tasks'])

<p class="h4 text-center">{{ "Задачи под номером $number" }}</p>
<ul class="list-unstyled">
    @foreach($tasks as $section => $tasks)
        <x-generator.tasks-by-section :section="$section" :tasks="$tasks"/>
    @endforeach
</ul>
