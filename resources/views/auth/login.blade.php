<x-layouts.guest>
    <x-slot name="title">Авторизация</x-slot>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <h1 class="h3 mb-3 fw-normal">Введите логин и пароль</h1>

        <div class="form-floating my-3">
            <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="example@example.com" value="{{ old('email') }}" required>
            <label for="email">Адрес электронной почты</label>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-floating my-3">
            <input id="password" type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="********" required>
            <label for="password">Пароль</label>

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-check text-start my-3">
            <label class="form-check-label" for="remember">Запомнить</label>
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        </div>

        <button class="btn btn-success w-100 py-2" type="submit">Войти</button>

        @if (Route::has('password.request'))
            <div class="my-3">
                <p><a class="link-success" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a></p>
            </div>
        @endif
    </form>
</x-layouts.guest>
