@props([
    'news'
])

<div class="container out mt-5">
    <div class="d-flex justify-content-between align-items-center">
        <p class="h5 text-start mt-3">Новости</p>
        <a href="{{ route('admin.news.create') }}" type="button" class="btn btn-primary btn-sm">Добавить запись</a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Идентификатор</th>
            <th class="col-2" scope="col">Картинка</th>
            <th scope="col">Заголовок</th>
            <th class="col-6" scope="col">Описание</th>
            <th class="col-1" scope="col">Дата</th>
        </tr>
        </thead>
        <tbody class="table-group-divider">
        @foreach($news as $item)
            <x-panels.admin.news-item :news="$item" />
        @endforeach
        </tbody>
    </table>
</div>
