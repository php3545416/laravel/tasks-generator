<?php

namespace App\Services;

use App\Contracts\TaskCreationServiceContract;
use App\Models\Task;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskCreationService implements TaskCreationServiceContract
{
    public function createTasksByTemplate(array $taskIds, array $idsWithCount): Collection
    {
        if (empty($taskIds) || empty($idsWithCount)) {
            throw new InvalidArgumentException('Не выбрана ни одна из задач!');
        }

        $tasks = Task::query()->whereIn('id', $taskIds)->withWhereHas('parameters')->get();

        if ($tasks->isEmpty()) {
            throw new NotFoundHttpException('Не найдены задачи или к их условиям не были привязаны переменные!');
        }

        return $this->generateTasks($tasks, $idsWithCount);
    }

    private function generateTasks(Collection $tasks, array $idsWithCount): Collection
    {
        $faker = fake();
        $idsWithCount = collect($idsWithCount)->keyBy('id');

        $id = 0;
        $generated = collect();
        foreach ($tasks as $task) {
            $count = $idsWithCount->get($task->id)['count'] ?? null;

            if (! $count) {
                continue;
            }

            for ($i = 1; $i <= $count; $i++) {
                $parameters = $task->parameters->toArray();

                foreach ($parameters as &$parameter) {
                    $parameter['value'] ??= $faker->numberBetween($parameter['rand_from'], $parameter['rand_to']);
                }

                $condition_params = Arr::pluck($parameters, 'value');
                $condition = sprintf($task->conditions_template, ...$condition_params);

                $ansTpl = sprintf($task->answer_template, ...$condition_params);
                $algTpl = sprintf($task->algorithm_template, ...$condition_params);

                $answer = math_eval($ansTpl);

                $algorithm_template = Str::contains($algTpl, 'answer')
                    ? Str::replace('answer', $answer, $algTpl)
                    : $algTpl;

                $newTask = (new Task())->forceFill([
                    'id' => $id,
                    'answer' => $answer,
                    'algorithm_template' => $algorithm_template,
                    'condition' => $condition,
                    'condition_params' => $condition_params,
                ]);

                $generated->add($newTask);

                $id++;
            }
        }

        return $generated;
    }
}
