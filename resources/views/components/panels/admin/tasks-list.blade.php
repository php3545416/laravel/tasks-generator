@props([
    'tasks'
])
<div class="container out mt-5">
    <div class="d-flex justify-content-between align-items-center">
        <p class="h5 text-start mt-3">Задачи</p>
        <a class="btn btn-primary btn-sm" href="{{ route('admin.tasks.create') }}">Добавить запись</a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Идентификатор</th>
            <th class="col-2" scope="col">Номер задания</th>
            <th scope="col">Задача</th>
        </tr>
        </thead>
        <tbody class="table-group-divider">
        @foreach($tasks as $task)
            <x-panels.admin.task-item :task="$task"/>
        @endforeach
        </tbody>
    </table>
</div>
