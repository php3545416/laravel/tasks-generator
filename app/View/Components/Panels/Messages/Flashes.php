<?php

namespace App\View\Components\Panels\Messages;

use Closure;
use Illuminate\Contracts\Session\Session;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Flashes extends Component
{
    public function __construct(private readonly Session $storage)
    {
    }

    public function render(): View|Closure|string
    {
        return view('components.panels.messages.flashes', [
            'error' => $this->storage->get('error_messages'),
            'success' => $this->storage->get('success_messages'),
        ]);
    }
}
