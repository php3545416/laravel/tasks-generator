@props([
    'header',
    'footer',
    'title',
    'mainSlot',
])
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<x-parts.head :title="$title ?? config('app.name', 'Laravel')"/>
<body>
    <x-parts.header />

    <div class="container my-4">
        <x-panels.messages.flashes />
    </div>

    @isset($mainSlot)
        {{ $mainSlot }}
    @else
        <main>
            {{ $slot ?? '' }}
        </main>
    @endisset

    <x-parts.footer />
</body>
</html>
