@props([
    'error',
    'success',
])

@if ($error)
    <x-panels.messages.error :message="$error" class="my-4"/>
@endif
@if ($success)
    <x-panels.messages.success :message="$success" class="my-4"/>
@endif
