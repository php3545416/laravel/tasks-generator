<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    use HasFactory;

    protected $appends = ['full_name'];

    public function getFullNameAttribute(): string
    {
        $additional = $this->rand_from ? "(от $this->rand_from до $this->rand_to $this->measure)" : '';

        return "$this->name $additional";
    }
}
