@props([
    'name',
])

<div class="form-group my-4">
    {{  $slot }}

    <x-panels.forms.error name="{{ $name }}" />
</div>
