<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function parameters(): BelongsToMany
    {
        return $this->belongsToMany(Parameter::class)
            ->withPivot('number')
            ->orderByPivot('number');
    }

    public function syncParameters(array $parameters): void
    {
        DB::transaction(function () use ($parameters) {
            $this->parameters()->detach();

            $this->parameters()->newPivotStatement()->insert(collect($parameters)->map(
                fn ($paramId, $number) => [
                    'task_id' => $this->id,
                    'parameter_id' => $paramId,
                    'number' => $number + 1,
                ])->all());
        });
    }
}
