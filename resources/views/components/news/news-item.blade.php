@props(['news'])

<tr>
    <td class="align-top" style="width: 150px;">
        <img src="{{ $news->image_url }}" class="img-thumbnail" alt="{{ $news->title }}">
    </td>
    <td>
        <div class="date">{{ 'Дата публикации: ' . $news->date  }}</div>
        <h5>{{ $news->title }}</h5>
        <p>{{ $news->description }}</p>
    </td>
</tr>
