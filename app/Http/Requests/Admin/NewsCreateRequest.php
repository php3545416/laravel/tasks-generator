<?php

namespace App\Http\Requests\Admin;

use ErlandMuchasaj\LaravelFileUploader\FileUploader;
use Illuminate\Foundation\Http\FormRequest;

class NewsCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'image' => [
                'nullable',
                'file',
                'image',
                'mimes:'.implode(',', FileUploader::images()),
                'max:'.(int) ini_get('upload_max_filesize') * 1000,
            ],
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:2000',
            'date' => 'required|date',
            'link' => 'required|url',
        ];
    }
}
