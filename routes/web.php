<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\NewsController as AdminNewsController;
use App\Http\Controllers\Admin\TaskController as AdminTaskController;
use App\Http\Controllers\CallbackController;
use App\Http\Controllers\GeneratorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * @Auth
 */
Auth::routes(['verify' => true]);

/**
 * @Public
 */
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/news', [NewsController::class, 'index'])->name('news.index');
Route::get('/callback', [CallbackController::class, 'index'])->name('callback.index');
Route::post('/callback', [CallbackController::class, 'store'])->name('callback.store');

Route::get('/generator', [GeneratorController::class, 'index'])->name('generator.index');
Route::post('/generator', [GeneratorController::class, 'store'])->name('generator.store');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile', [ProfileController::class, 'show'])->name('profile.index');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
});

/**
 * @Admin
 */
Route::middleware('admin')
    ->prefix('admin')
    ->name('admin.')
    ->group(function () {
        Route::get('/', [AdminController::class, 'index'])->name('index');
        Route::resource('tasks', AdminTaskController::class)->except('show', 'index');
        Route::get('/tasks/linked-parameter-view', [AdminTaskController::class, 'getLinkParameterView']);

        Route::resource('/news', AdminNewsController::class)->except('show', 'index');
    });
